console.warn(`JS DOM Manipulation`);

//alternative query selectors
	//document.getElementById(`txt-first-name`)
	//document.getElementsByClassName(`txt-inputs`)
	//document.getElementsByTagName(`input`)

//retrieve elems fr webpage

let fName = document.querySelector(`#txt-first-name`);
let lName = document.querySelector(`#txt-last-name`);
let fullName = document.querySelector(`#txt-full-name`)

addEventListener(`keyup`, (event) => {
	fullName.innerHTML = `${fName.value} ${lName.value}`
});

//multiple listeners can also be assigned to same event
fName.addEventListener(`keyup`, (event) => {
	console.log(event.target); //access the object model
	console.log(event.target.value); //access the specific value
});

//---------------------------------------------------------------

//THIS IS SESSION 43: REACTIVE DOM with JSON

let posts = [];
let count = 1;

//Add post data

document.querySelector(`#form-add-post`).addEventListener(`submit`, (event) => {
		event.preventDefault(); //to avoid refreshing of form
		posts.push({
			id: count,
			title: document.querySelector(`#txt-title`).value,
			body: document.querySelector(`#txt-body`).value
		})

		count++;
		alert(`Successfully added`);
		console.log(posts);
		showPowsts(posts);
});

//show posts
let showPowsts = (post) => {
	let postEntries = ``;

	post.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>

				<button>Edit</button>
				<button>Delete</button>
			</div>
		`
	})

	document.querySelector(`#div-post-entries`).innerHTML = postEntries;
};